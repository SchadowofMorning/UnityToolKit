﻿
using UnityEngine;
using System;
using System.Collections.Generic;

#pragma warning disable 2235
[Node("Noise/Perlin", typeof(PerlinNoiseNode), typeof(Node)), Serializable]
public class PerlinNoiseNode : Node
{
    [Handle(typeof(float[,]), HandleDirection.OUT, false)]
    public float[,] values;
    [Handle(typeof(int), HandleDirection.IN, false)]
    public float perlinScale = 0.05f;
    [Handle(typeof(int), HandleDirection.IN, false)]
    public int size;
    [Handle(typeof(float), HandleDirection.IN, false)]
    public float xPosition;
    [Handle(typeof(float), HandleDirection.IN, false)]
    public float yPosition;
    public PerlinNoiseNode()
    {
        rect = new Rect(rect.position, new Vector2(100, 100));
        size = 128;
        menuItems = new List<Tuple<string, Action>>();
    }
    public override void Draw()
    {
        CollectInputs();
        if (values == null) Recalculate();
        base.Draw();
        if (currentPreview == null) currentPreview = Preview;
        if(currentPreview != null)
        GUI.Box(rect, new GUIContent(currentPreview), NodeEditor.nodeStyle);
    }
    public override bool Recalculate()
    {
        
        values = new float[size, size];
        for (int x = 0; x < size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                values[x, y] = Mathf.PerlinNoise((xPosition + x) * perlinScale, (yPosition + y) * perlinScale);
            }
        }
        currentPreview = Preview;
        GUI.changed = true;
        return true;
    }
    protected Texture2D currentPreview;
    public Texture2D Preview
    {
        get
        {
            int previewsize = size < 128 ? size : 128;
            Texture2D _tex = new Texture2D(previewsize, previewsize);
            for (int x = 0; x < previewsize; x++)
            {
                for (int y = 0; y < previewsize; y++)
                {
                    float f = values[x, y];
                    _tex.SetPixel(x, y, new Color(f,f,f));
                }
            }
            _tex.Apply();
            return _tex;
        }
    }
}