﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Simplex;
[Node("Noise/Simplex", typeof(SimplexNoiseNode), typeof(Node))]
public class SimplexNoiseNode : Node
{
    [Handle(typeof(float[,]), HandleDirection.OUT, false)]
    public float[,] values;
    [Handle(typeof(int), HandleDirection.IN, false)]
    public float scale = 0.05f;
    [Handle(typeof(int), HandleDirection.IN, false)]
    public int size;
    [Handle(typeof(float), HandleDirection.IN, false)]
    public float xPosition;
    [Handle(typeof(float), HandleDirection.IN, false)]
    public float yPosition;
    public SimplexNoiseNode()
    {
        rect = new Rect(rect.position, new Vector2(100, 100));
        size = 128;
        menuItems = new List<Tuple<string, Action>>();
    }
    public override void Draw()
    {
        CollectInputs();
        if (values == null) Recalculate();
        base.Draw();
        if (currentPreview == null) currentPreview = Preview;
        if (currentPreview != null)
            GUI.Box(rect, new GUIContent(currentPreview), NodeEditor.nodeStyle);
    }
    public override bool Recalculate()
    {

        values = new float[size, size];
        for (int x = 0; x < values.GetUpperBound(0) + 1; x++)
        {
            for (int y = 0; y < values.GetUpperBound(1) + 1; y++)
            {
                values[x, y] = Noise.CalcPixel2D(Mathf.RoundToInt(xPosition + x),Mathf.RoundToInt(yPosition + y), scale) / 255;
            }
        }
        
        currentPreview = Preview;
        GUI.changed = true;
        return true;
    }
    protected Texture2D currentPreview;
    public Texture2D Preview
    {
        get
        {
            int previewsize = size < 128 ? size : 128;
            Texture2D _tex = new Texture2D(previewsize, previewsize);
            for (int x = 0; x < previewsize; x++)
            {
                for (int y = 0; y < previewsize; y++)
                {
                    float f = values[x, y];
                    _tex.SetPixel(x, y, new Color(f, f, f));
                }
            }
            _tex.Apply();
            return _tex;
        }
    }
    protected override void ProcessContextMenu()
    {
        base.ProcessContextMenu();
    }
}