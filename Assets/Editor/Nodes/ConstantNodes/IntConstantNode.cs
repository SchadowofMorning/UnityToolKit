﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[Node("Constant/Int", typeof(IntConstantNode), typeof(Node))]
public class IntConstantNode : ConstantNode
{
    [Handle(typeof(int), HandleDirection.OUT, false, true)]
    public int constant;
    public override object value
    {
        get
        {
            return constant;
        }
        set
        {
            constant = (int)value;
        }
    }
}


