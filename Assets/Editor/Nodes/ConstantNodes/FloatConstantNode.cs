﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[Node("Constant/Float", typeof(FloatConstantNode), typeof(Node))]
public class FloatConstantNode : ConstantNode
{
    [Handle(typeof(float), HandleDirection.OUT, false, true)]
    public float constant;
    public override object value
    {
        get
        {
            return constant;
        }
        set
        {
            if(value != null)
            constant = (float)value;
        }
    }
}


