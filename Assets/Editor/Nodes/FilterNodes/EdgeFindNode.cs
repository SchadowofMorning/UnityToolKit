﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[Node("Matrix/EdgeFind", typeof(EdgeFindNode), typeof(Node))]
public class EdgeFindNode : MatrixNode
{
    const float outside = -1f;
    const float inside = 8f;
    static float[,] kernel
    {
        get
        {
            return new float[,] { { outside,outside,outside }, { outside, inside, outside }, { outside, outside, outside } };
        }
    }
    public override bool Recalculate()
    {
        if (!CollectInputs()) return false;
        else
        {
            output = new float[input.GetLength(0), input.GetLength(1)];
            if (input == null) return false;
            for (int x = 0; x < input.GetLength(0); x++)
            {
                for (int y = 0; y < input.GetLength(1); y++)
                {
                    float sum = 0f;
                    for (int i = 2; i > -1; i--)
                    {
                        for (int j = 2; j > -1; j--)
                        {
                            float w = kernel[i, j];
                            sum += w * input[Mathf.Clamp(i + x, 0, input.GetUpperBound(0)),Mathf.Clamp(j + y, 0, input.GetUpperBound(1))];
                        }
                    }
                    output[x, y] = sum;
                }
            }
            return true;
        }
    }
}