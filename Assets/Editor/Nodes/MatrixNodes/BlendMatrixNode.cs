﻿
using System;

using UnityEngine;
[Node("Matrix/Blend", typeof(BlendMatrixNode), typeof(Node)), Serializable]
public class BlendMatrixNode : Node
{
    [Handle(typeof(float[,]), HandleDirection.IN, false, true)]
    public float[,] input_1;
    [Handle(typeof(float[,]), HandleDirection.IN, false, true)]
    public float[,] input_2;
    [Handle(typeof(float[,]), HandleDirection.OUT, false, false)]
    public float[,] output;
    [Handle(typeof(float), HandleDirection.IN, false)]
    public float ratio = 0.5f;
    [MapFunction("Recalculate")]
    public override bool Recalculate()
    {
        if (!CollectInputs()) return false;
        output = new float[input_1.GetUpperBound(0) + 1, input_1.GetUpperBound(1) + 1];
        for (int x = 0; x < output.GetUpperBound(0) + 1; x++)
        {
            for (int y = 0; y < output.GetUpperBound(1) + 1; y++)
            {
                output[x, y] = (input_1[x, y] * (1 - ratio) + input_2[x, y] * ratio);
            }
        }
        return true;
    }
#if UNITY_EDITOR
    public override void Draw()
    {
        base.Draw();
        GUI.Box(rect, new GUIContent(Preview), NodeEditor.nodeStyle);
    }
    protected Texture2D Preview
    {
        get
        {
            if (output == null) return new Texture2D(1, 1);
            else
            {
                Texture2D _tex = new Texture2D(output.GetUpperBound(0), output.GetUpperBound(1));
                for (int x = 0; x < output.GetUpperBound(0); x++)
                {
                    for (int y = 0; y < output.GetUpperBound(1); y++)
                    {
                        float f = output[x, y];
                        _tex.SetPixel(x, y, new Color(f, f, f));
                    }
                }
                _tex.Apply();
                return _tex;
            }
        }
    }
#endif
}
