﻿using System;
using UnityEngine;
[Node("Matrix/Invert", typeof(InvertNode), typeof(Node)), Serializable]
public class InvertNode : MatrixNode
{
    public override bool Recalculate()
    {
        CollectInputs();
        if (input == null) return false;
        output = new float[input.GetUpperBound(0) + 1, input.GetUpperBound(1) + 1];
        for (int x = 0; x < output.GetUpperBound(0) + 1; x++)
        {
            for (int y = 0; y < output.GetUpperBound(1) + 1; y++)
            {
                output[x, y] = 1 - input[x, y];
            }
        }
        return true;
    }
}