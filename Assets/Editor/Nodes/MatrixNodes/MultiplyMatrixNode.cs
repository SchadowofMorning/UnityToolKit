﻿using System;
using UnityEngine;
[Node("Matrix/Multiply", typeof(MultiplyMatrixNode), typeof(Node)), Serializable]
public class MultiplyMatrixNode : Node
{
    [Handle(typeof(float[,]), HandleDirection.IN, false, true)]
    public float[,] input;
    [Handle(typeof(float[,]), HandleDirection.OUT, false)]
    public float[,] output;
    public float value = 1f;
    public override bool Recalculate()
    {
        if (!CollectInputs()) return false;
        if (input == null) return false;
        output = new float[input.GetUpperBound(0) + 1, input.GetUpperBound(1) + 1];
        for (int x = 0; x < output.GetUpperBound(0) + 1; x++)
        {
            for (int y = 0; y < output.GetUpperBound(1) + 1; y++)
            {
                output[x, y] = input[x, y] * value;
            }
        }
        return true;
    }
#if UNITY_EDITOR
    public override void Draw()
    {
        base.Draw();
        GUI.Box(rect, new GUIContent(preview), NodeEditor.nodeStyle);
    }
    protected Texture2D preview
    {
        get
        {
            if (output == null) return new Texture2D(1, 1);
            else
            {
                Texture2D _tex = new Texture2D(output.GetUpperBound(0), output.GetUpperBound(1));
                for (int x = 0; x < output.GetUpperBound(0); x++)
                {
                    for (int y = 0; y < output.GetUpperBound(1); y++)
                    {
                        float f = output[x, y];
                        _tex.SetPixel(x, y, new Color(f, f, f));
                    }
                }
                _tex.Apply();
                return _tex;
            }
        }
    }
#endif
}