﻿
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
[Node("Matrix/Power", typeof(PowerNode), typeof(Node)), Serializable]
public class PowerNode : MatrixNode
{
    public float value = 1f;
    public bool invert;
    public override bool Recalculate()
    {
        if (!CollectInputs()) return false;
        output = new float[input.GetUpperBound(0) + 1, input.GetUpperBound(1) + 1];
        for (int x = 0; x < output.GetUpperBound(0) + 1; x++)
        {
            for (int y = 0; y < output.GetUpperBound(1) + 1; y++)
            {
                if (invert)
                    output[x, y] = Mathf.Pow(value, input[x, y]);
                else
                    output[x, y] = Mathf.Pow(input[x, y], value);
            }
        }
        return true;
    }
}