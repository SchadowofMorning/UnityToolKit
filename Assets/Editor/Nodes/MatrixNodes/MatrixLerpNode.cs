﻿using System;
using UnityEngine;
[Node("Matrix/Lerp", typeof(MatrixLerpNode), typeof(Node))]
public class MatrixLerpNode : MatrixNode
{
    [Handle(typeof(float), HandleDirection.IN, false, false)]
    public float lerpValue;
    public override bool Recalculate()
    {
        if (!CollectInputs()) return false;
        output = new float[input.GetUpperBound(0) + 1, input.GetUpperBound(0) + 1];
        for (int x = 0; x < output.GetUpperBound(0) + 1; x++)
        {
            for (int y = 0; y < output.GetUpperBound(1) + 1; y++)
            {
                float f = input[x, y];
                if (f > 0.5f)
                    output[x, y] = Mathf.Lerp(f, 1f, lerpValue);
                else
                    output[x, y] = Mathf.Lerp(f, 0f, lerpValue);
            }
        }
        return true;
    }
}