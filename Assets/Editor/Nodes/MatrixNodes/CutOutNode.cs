﻿using UnityEngine;

[Node("Matrix/CoutOutNode", typeof(CutOutNode), typeof(Node))]
public class CutOutNode : MatrixNode
{
    [Handle(typeof(float[,]), HandleDirection.IN, false, true)]
    public float[,] cutOutMap;
    [Handle(typeof(float), HandleDirection.IN, false, false)]
    public float min;
    [Handle(typeof(float), HandleDirection.IN, false, false)]
    public float max;
    [Handle(typeof(float), HandleDirection.IN, false, false)]
    public float value;
#if UNITY_EDITOR
    public override void Draw()
    {
        base.Draw();
        GUI.Box(rect, new GUIContent(preview), NodeEditor.nodeStyle);
    }
#endif
    public override bool Recalculate()
    {
        
        if (!CollectInputs()) return false;
        if (input == null) return false;
        output = new float[input.GetUpperBound(0) + 1, input.GetUpperBound(1) + 1];
        for (int x = 0; x < input.GetUpperBound(0) + 1; x++)
        {
            for (int y = 0; y < input.GetUpperBound(1) + 1; y++)
            {
                output[x, y] = min < cutOutMap[x, y] && max > cutOutMap[x, y] ? input[x, y] : value;
            }
        }
        return true;
    }
}