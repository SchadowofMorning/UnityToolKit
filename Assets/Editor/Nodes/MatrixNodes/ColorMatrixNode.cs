﻿using UnityEngine;

using System.Collections.Generic;
using System;
/// <summary>
/// Colorcoding, because what are we, Animals? -Pidge
/// </summary>
[Node("Matrix/Colorcoding", typeof(ColorMatrixNode), typeof(Node))]
public class ColorMatrixNode : Node
{
    [Handle(typeof(float[,]), HandleDirection.IN, false, true)]
    public float[,] input;
    public Gradient gradient;
    public ColorMatrixNode()
    {
        rect = new Rect(rect.position, new Vector2(100, 100));
        menuItems = new List<Tuple<string, Action>>();
        
    }
#if UNITY_EDITOR
    public override void Draw()
    {
        base.Draw();
        if (input != null) currentPreview = Preview;
        if (currentPreview != null)
            GUI.Box(rect, new GUIContent(currentPreview), NodeEditor.nodeStyle);
        else
            GUI.Box(rect, new GUIContent(""));
    }
    protected Texture2D currentPreview;
    public Texture2D Preview
    {
        get
        {
            if (gradient == null) return null;
            int previewsize = input.GetUpperBound(0) + 1 < 128 ? input.GetUpperBound(1) + 1 : 128;
            Texture2D _tex = new Texture2D(previewsize, previewsize);
            for (int x = 0; x < previewsize; x++)
            {
                for (int y = 0; y < previewsize; y++)
                {
                    float f = input[x, y];
                    
                    _tex.SetPixel(x, y, gradient.Evaluate(f));
                }
            }
            _tex.Apply();
            return _tex;
        }
    }
#endif
}