﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
[Node("Matrix/Bulge", typeof(BulgeNode), typeof(Node))]
public class BulgeNode : MatrixNode
{
    [Handle(typeof(float), HandleDirection.IN, false, false)]
    public float max;
    [Handle(typeof(float), HandleDirection.IN, false, false)]
    public float m;
    [Handle(typeof(float), HandleDirection.IN, false, false)]
    public float b;
    public override bool Recalculate()
    {
        if (!CollectInputs()) return false;
        if (input == null) return false;
        output = (float[,])input.Clone();
        Vector2 center = new Vector2(input.GetUpperBound(0), input.GetUpperBound(1)) / 2f;
        float maxDist = Vector2.Distance(Vector2.zero, center);
        for (int x = 0; x < input.GetLength(0); x++)
        {
            for (int y = 0; y < input.GetLength(1); y++)
            {
                Vector2 point = new Vector2(x, y);
                float d = Vector2.Distance(point, center);
                float p = Mathf.Lerp(0, max, d / maxDist);
                float val = (m * Mathf.Pow(p, 2)) + b;
                output[x, y] = val * input[x,y];
            }
        }
        return true;
    }
}