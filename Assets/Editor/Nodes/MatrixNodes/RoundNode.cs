﻿using System;
using System.Collections.Generic;
using UnityEngine;
[Node("Matrix/Round", typeof(RoundNode), typeof(Node)), Serializable]
public class RoundNode : Node
{
    [Handle(typeof(float[,]), HandleDirection.IN, false, true)]
    public float[,] input;
    [Handle(typeof(float[,]), HandleDirection.OUT, false)]
    public float[,] output;
    public int value = 0;
    [MapFunction("Recalculate")]
    public override bool Recalculate()
    {
        if (!CollectInputs()) return false;
        output = new float[input.GetUpperBound(0) + 1, input.GetUpperBound(1) + 1];
        for (int x = 0; x < output.GetUpperBound(0) + 1; x++)
        {
            for (int y = 0; y < output.GetUpperBound(1) + 1; y++)
            {
                output[x, y] = (float)Math.Round(input[x, y] * value) / value;
            }
        }
        return true;
    }
#if UNITY_EDITOR
    public override void Draw()
    {
        base.Draw();
        GUI.Box(rect, new GUIContent(Preview), NodeEditor.nodeStyle);
    }
    protected Texture2D Preview
    {
        get
        {
            if (output == null) return new Texture2D(1, 1);
            else
            {
                Texture2D _tex = new Texture2D(output.GetUpperBound(0), output.GetUpperBound(1));
                for (int x = 0; x < output.GetUpperBound(0); x++)
                {
                    for (int y = 0; y < output.GetUpperBound(1); y++)
                    {
                        float f = output[x, y];
                        _tex.SetPixel(x, y, new Color(f, f, f));
                    }
                }
                _tex.Apply();
                return _tex;
            }
        }
    }
#endif
}