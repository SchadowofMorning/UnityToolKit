﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;


[Node("CellularAutomaton/Basic", typeof(CellularAutomataNode), typeof(Node)), Serializable]
public class CellularAutomataNode : Node
{
    FastNoise noise;
    [Handle(typeof(float[,]), HandleDirection.OUT, false)]
    public float[,] values;
    [Handle(typeof(int), HandleDirection.IN, false)]
    public int size;
    [Handle(typeof(int), HandleDirection.IN, false)]
    public int seed = 1337;
    [Handle(typeof(int), HandleDirection.IN, false)]
    public int scalar = 1;
    [Handle(typeof(int), HandleDirection.IN, false)]
    public int xPosition = 0;
    [Handle(typeof(int), HandleDirection.IN, false)]
    public int yPosition = 0;
    public CellularAutomataNode()
    {
        rect = new Rect(rect.position, new Vector2(100, 100));
        size = 128;
        menuItems = new List<Tuple<string, Action>>();
    }
    public override void Draw()
    {
        CollectInputs();
        if (values == null) Recalculate();
        base.Draw();
        if (currentPreview == null) currentPreview = Preview;
        if (currentPreview != null)
            GUI.Box(rect, new GUIContent(currentPreview), NodeEditor.nodeStyle);
    }
    public override bool Recalculate()
    {
        noise = new FastNoise(seed);
        values = new float[size, size];
        for (int x = 0; x < size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                values[x, y] = noise.GetCellular((xPosition + x) * scalar,(yPosition + y) * scalar);
            }
        }
        currentPreview = Preview;
        GUI.changed = true;
        return true;
    }
    protected Texture2D currentPreview;
    public Texture2D Preview
    {
        get
        {
            int previewsize = size < 128 ? size : 128;
            Texture2D _tex = new Texture2D(previewsize, previewsize);
            for (int x = 0; x < previewsize; x++)
            {
                for (int y = 0; y < previewsize; y++)
                {
                    float f = values[x, y];
                    _tex.SetPixel(x, y, new Color(f, f, f));
                }
            }
            _tex.Apply();
            return _tex;
        }
    }
}