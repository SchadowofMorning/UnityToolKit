﻿
using System;
using System.Reflection;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
#if UNITY_EDITOR
public class NodeEditor : EditorWindow
{
    public static NodeEditor editor;
    public static Node selectedNode;
    public static NodeCanvas canvas;
    protected List<NodeAttribute> nodeTypes;
    public static Handle drawingHandle;
    public static bool redraw = false;
    private bool dragging;
    public static GUIStyle nodeStyle;
    public static GUIStyle handleStyle;
    private Vector2 offset;
    private Vector2 drag;
    [MenuItem("NodeEditor/StandardEditor")]
    static void Init()
    {
        if (editor != null) Destroy(editor);
        editor = GetWindow<NodeEditor>();
    }
    protected virtual void OnGUI()
    {

        //DrawGrid(20, 0.2f, Color.gray);
        //DrawGrid(100, 0.4f, Color.gray);
        if (nodeStyle == null)
        {
            nodeStyle = GUI.skin.box;
            nodeStyle.alignment = TextAnchor.MiddleCenter;
        }
        if (handleStyle == null) handleStyle = GUI.skin.box;
        DrawToolBar();
        if (editor == null) editor = this;
        if (canvas != null && canvas.baseTypes != null)
        {
            HandleEvents(Event.current);
            canvas.Draw();
            DrawNewConnection(Event.current);

            if (nodeTypes == null) nodeTypes = canvas.GetNodesOfBaseType();

            if (redraw)
            {
                if(canvas.changed)canvas.Recalculate();
                redraw = false;
                GUI.changed = true;
            }
        }
    }
    public void DrawToolBar()
    {
        EditorGUILayout.BeginHorizontal(EditorStyles.toolbar); //<body class="horizontal">
        EditorGUILayout.BeginVertical();
        NodeCanvas updated = (NodeCanvas)EditorGUILayout.ObjectField(canvas, typeof(NodeCanvas), false);
        if(updated != canvas)
        {
            canvas = updated;
            if(canvas != null)
                EditorUtility.SetDirty(canvas);
        }
        EditorGUILayout.EndVertical();
        if (GUILayout.Button(new GUIContent("clear all"), EditorStyles.miniButton))
        {
            if (canvas != null)
            {
                canvas.RemoveAll();
            }
        }
        if (GUILayout.Button(new GUIContent("save"), EditorStyles.miniButton))
            canvas.SaveCanvas();
        if (GUILayout.Button(new GUIContent("recalculate"), EditorStyles.miniButton))
            canvas.Recalculate();
        EditorGUILayout.EndHorizontal(); //</body>
    }
    private void DrawGrid(float gridSpacing, float gridOpacity, Color gridColor)
    {
        int widthDivs = Mathf.CeilToInt(position.width / gridSpacing);
        int heightDivs = Mathf.CeilToInt(position.height / gridSpacing);

        Handles.BeginGUI();
        Handles.color = new Color(gridColor.r, gridColor.g, gridColor.b, gridOpacity);

        offset += drag * 0.5f;
        Vector3 newOffset = new Vector3(offset.x % gridSpacing, offset.y % gridSpacing, 0);

        for (int i = 0; i < widthDivs; i++)
        {
            Handles.DrawLine(new Vector3(gridSpacing * i, -gridSpacing, 0) + newOffset, new Vector3(gridSpacing * i, position.height, 0f) + newOffset);
        }

        for (int j = 0; j < heightDivs; j++)
        {
            Handles.DrawLine(new Vector3(-gridSpacing, gridSpacing * j, 0) + newOffset, new Vector3(position.width, gridSpacing * j, 0f) + newOffset);
        }

        Handles.color = Color.white;
        Handles.EndGUI();
    }
    protected virtual void HandleEvents(Event e)
    {
        if (canvas.HandleEvents(e))
        {
            redraw = true;
        }
        else
        {
            switch (e.type)
            {
                case EventType.MouseDown:
                    if (e.button == 0)
                    {
                        Selection.activeObject = null;
                        dragging = true;
                    }
                    else if (e.button == 1)
                    {
                        DrawContextMenu(e.mousePosition);
                    }
                    else
                        Selection.activeObject = null;
                    break;
                case EventType.MouseDrag:
                    if(dragging == true && drawingHandle == null)
                    {
                        Drag(e.delta);
                        e.Use();
                    }
                    break;
                case EventType.MouseUp:
                    dragging = false;
                    break;
            }
        }
    }
    protected void Drag(Vector2 delta)
    {
        drag += delta;
        GUI.changed = true;
        if (canvas != null)
            canvas.Drag(delta);
    }
    protected virtual void DrawContextMenu(Vector2 vector)
    {
        GenericMenu menu = new GenericMenu();
        if (canvas != null)
            foreach (NodeAttribute t in nodeTypes) menu.AddItem(new GUIContent(t.menuItem), false, () => canvas.AddNode(GetNode(t.type, vector)));
        menu.AddSeparator("");
        menu.AddItem(new GUIContent("Save"), false, () => canvas.SaveCanvas());
        menu.ShowAsContext();
    }
    protected void DrawNewConnection(Event e)
    {
        if (drawingHandle != null)
        {
            Vector2 start = drawingHandle.rect.center;
            Vector2 end = e.mousePosition;
            Vector2 startTan = start - Vector2.left * 50f;
            Vector2 endTan = end + Vector2.left * 50f;
            Handles.DrawBezier(start, end, startTan, endTan, Color.green, null, 2f);
            redraw = true;
            if (e.type == EventType.MouseDown && e.button == 1) drawingHandle = null;
            if (e.type == EventType.MouseDrag) e.Use();
        }
    }
    public Node GetNode(Type t, Vector2 pos)
    {
        Node n = (Node)CreateInstance(t);
        n.rect.position = pos;
        n.canvas = canvas;
        redraw = true;
        this.Repaint();
        return n;
    }
}
#endif