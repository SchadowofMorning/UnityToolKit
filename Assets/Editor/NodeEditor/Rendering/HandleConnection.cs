﻿
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

[Serializable]
public class HandleConnection : ScriptableObject
{
    public Handle origin;
    public Handle target;
    public string path;
    public HandleConnection()
    {

    }
    public static HandleConnection Create(Handle o, Handle t)
    {
        HandleConnection connection = CreateInstance<HandleConnection>();
        connection.origin = o;
        connection.target = t;
        o.parent.canvas.changed = true;
        return connection;
    }
#if UNITY_EDITOR
    public void Draw()
    {
        Vector2 startPoint = origin.rect.center;
        Vector2 endPoint = target.rect.center;
        Vector2 startDiff = startPoint - Vector2.left * 50f;
        Vector2 endDiff = endPoint + Vector2.left * 50f;
        Handles.DrawBezier(startPoint, endPoint, startDiff, endDiff, Color.black, null, 2f);
        GUI.changed = true;
        if(Handles.Button((startPoint + endPoint) * 0.5f, Quaternion.identity, 4, 8, Handles.RectangleHandleCap))
        {
            target.RemoveConnection(this);
        }
    }
    public void DestroyConnection()
    {
        origin.parent.canvas.changed = true;
        origin.RemoveConnection(this);
        target.RemoveConnection(this);
    }
#endif
}