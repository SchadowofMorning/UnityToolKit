﻿using UnityEngine;


public abstract class ConstantNode : Node
{
    public string constantName;
    protected object oldValue;
    public abstract object value
    {
        get;
        set;
    }
    public void ChangeValue(object val)
    {
        oldValue = value;
        value = val;
    }
    public void Revert()
    {
        value = oldValue;
    }
    public ConstantNode() : base()
    {
        rect = new Rect(0, 0, 100, 40);
    }
    public override void Draw()
    {
        base.Draw();
        Recalculate();
        GUI.Box(rect, new GUIContent(constantName + "\n" + value.ToString()));
    }
}