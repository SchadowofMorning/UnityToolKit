﻿
using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;
using UnityEditor;
using System.Reflection;

#pragma warning disable 2235

[Serializable]
public abstract class Node : ScriptableObject
{
    [HideInInspector]
    public Rect rect;
    [NonSerialized]
    private bool dragged;
    [NonSerialized]
    protected List<Tuple<string, Action>> menuItems;
    [HideInInspector]
    public string path;
    [HideInInspector]
    public List<Handle> handles;
    [HideInInspector]
    public NodeCanvas canvas;
    public Node()
    {
        rect = new Rect(0, 0, 100, 100);
        dragged = false;
        menuItems = new List<Tuple<string, Action>>();
    }
    protected List<Handle> GetHandleFields()
    {
        IEnumerable<FieldInfo> properties = this.GetType().GetFields().Where(x => x.IsDefined(typeof(HandleAttribute)));
        List<Handle> result = new List<Handle>();
        int leftOrder = 0;
        int rightOrder = 0;
        foreach (FieldInfo fi in properties)
        {
            Handle h = Handle.Create(fi, fi.GetCustomAttribute<HandleAttribute>(), this);
            if (h.direction == HandleDirection.IN)
            {
                h.rect.position = rect.position + new Vector2(-80, (20 * leftOrder));
                leftOrder++;
            }
            else
            {
                h.rect.position = rect.position + new Vector2(rect.width, (20 * rightOrder));
                rightOrder++;
            }
            result.Add(h);
        }
        return result;
    }
#if UNITY_EDITOR
    public virtual void Draw()
    {
        if (handles == null || handles.Count == 0) handles = GetHandleFields();
        DrawHandles();
    }
    protected void DrawHandles()
    {
        foreach (Handle h in handles) { h.Draw(); }
    }
    public void Drag(Vector2 delta)
    {
        rect.position += delta;
        foreach (Handle handle in handles)
        {
            handle.rect.position += delta;
        }
    }
    protected virtual void ProcessContextMenu()
    {
        AddAction("Delete", () => { NodeEditor.canvas.DeleteNode(this); NodeEditor.redraw = true; });
        GenericMenu menu = new GenericMenu();
        foreach (Tuple<string, Action> action in menuItems)
        {
            menu.AddItem(new GUIContent(action.Item1), false, action.Item2.Invoke);
        }
        menu.ShowAsContext();
    }
    public bool HandleEvents(Event e)
    {
        if (handles != null)
            foreach (Handle h in handles)
                h.HandleEvents(e);
        switch (e.type)
        {
            case EventType.MouseDown:
                if (rect.Contains(e.mousePosition))
                {
                    if (e.button == 0)
                    {
                        //canvas.changed = true;
                        dragged = true;
                        Selection.activeObject = this;
                    }
                    else
                    {
                        ProcessContextMenu();
                    }
                    return true;
                }
                else return false;

            case EventType.MouseDrag:
                if (dragged)
                {
                    //canvas.changed = true;
                    Drag(e.delta);
                    e.Use();
                    return true;
                }
                else return false;
            case EventType.MouseUp:
                dragged = false;
                return false;
        }

        return false;
    }
    public void AddAction(string s, Action a)
    {
        menuItems.Add(new Tuple<string, Action>(s, a));
    }
    public void OnValidate()
    {
        Recalculate();
    }
#endif
    public virtual bool Recalculate()
    {
        if (CollectInputs()) return false;
        else return true;
    }
    public bool CollectInputs()
    {
        if (handles == null) handles = GetHandleFields();
        bool trigger = true;
        foreach (FieldInfo fieldInfo in GetType().GetFields().Where(x => x.GetCustomAttribute<HandleAttribute>() != null))
        {
            Handle handle = handles.FirstOrDefault(x => x.id == fieldInfo.Name);
            if (handle == null) continue;
            if (handle.direction == HandleDirection.IN)
                if (handle.Connected)
                {
                    object value = handle.GET();
                    if (value == null) trigger = false;
                    else
                    fieldInfo.SetValue(this, handle.GET());
                }
                else if (handle.required)
                {
                    trigger = false;
                }
        }
        return trigger;
    }
}
