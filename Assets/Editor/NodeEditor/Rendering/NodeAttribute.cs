﻿#if UNITY_EDITOR
using System;

[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
public class NodeAttribute : Attribute
{
    public string menuItem;
    public Type type;
    public Type baseType;
    public NodeAttribute(string mi, Type t, Type ct)
    {
        menuItem = mi;
        type = t;
        baseType = ct;
    }
}
#endif