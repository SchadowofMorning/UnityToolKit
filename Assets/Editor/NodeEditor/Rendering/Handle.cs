﻿
using UnityEngine;
using System.Collections.Generic;
using System;
using System.Reflection;
using System.Linq;

#pragma warning disable 2235

[Serializable]
public class Handle : ScriptableObject
{
    public string id;
    public Rect rect;
    [SerializeField]
    protected string content;
    public List<HandleConnection> connections;
    public HandleDirection direction;
    public bool multi;
    [SerializeField]
    public Node parent;
    [SerializeField]
    protected string type;
    public string path;
    public bool required;
    public bool Connected
    {
        get
        {
            return connections.Count > 0;
        }
    }
    public Handle()
    {
        connections = new List<HandleConnection>();
        rect = new Rect(0, 0, 80, 20);
    }
    public static Handle Create(FieldInfo fi, HandleAttribute ha, Node p)
    {
        Handle handle = CreateInstance<Handle>();
        handle.content = fi.Name + ">";
        handle.direction = ha.direction;
        handle.parent = p;
        handle.multi = ha.allowMultiple;
        handle.type = fi.FieldType.ToString();
        handle.id = fi.Name;
        handle.connections = new List<HandleConnection>();
        handle.required = ha.required;
        return handle;
    }
    public object GET()
    {
        try
        {
            return connections.Count > 0 ? connections[0].origin.GET() : parent.GetType().GetFields().First(x => x.Name == id).GetValue(parent);
        } catch(Exception e)
        {
            Debug.Log(e.Message);
            return null;
        }
    }
#if UNITY_EDITOR
    public virtual void Draw()
    {
        
        GUI.Box(rect, content, NodeEditor.handleStyle);
        for (int i = 0; i < connections.Count; i++)
        {
            try
            {
                connections[i].Draw();
            } catch(Exception e)
            {
                Debug.Log(name);
                Debug.Log(e.Message);
                connections = new List<HandleConnection>();
            }
        }
    }
    
    public void AddConnection(Handle handle)
    {
        if (handle == this) return;
        if (type != handle.type && handle.type != "System.Object" && type != "System.Object")
        {
            Debug.Log(type);
            Debug.Log(handle.type);
            return;
        }
        if (multi)
            connections.Add(HandleConnection.Create(handle, this));
        else
        if (connections.Count == 1) connections[0] = HandleConnection.Create(handle, this);
        else
        {
            connections.Add(HandleConnection.Create(handle, this));
        }
    }
    public void RemoveConnection(HandleConnection hc)
    {
        connections.Remove(hc);
    }
    public bool HandleEvents(Event e)
    {
        switch (e.type)
        {
            case EventType.MouseDown:
                if (rect.Contains(e.mousePosition))
                {
                    if (e.button == 0 && direction == HandleDirection.OUT)
                    {
                        parent.canvas.changed = true;
                        NodeEditor.drawingHandle = this;
                    }
                    return true;
                }
                else return false;
            case EventType.MouseUp:
                if(rect.Contains(e.mousePosition) && NodeEditor.drawingHandle != null)
                {
                    parent.canvas.changed = true;
                    NodeEditor.redraw = true;
                    this.AddConnection(NodeEditor.drawingHandle);
                    NodeEditor.drawingHandle = null;
                }
                return false;
        }
        return false;
    }
#endif
}