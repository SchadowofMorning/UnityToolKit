﻿#if UNITY_EDITOR
using System;

[AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
public class HandleAttribute : Attribute
{
    public Type type;
    public HandleDirection direction;
    public bool allowMultiple;
    public bool required;
    public HandleAttribute(Type t, HandleDirection hd, bool allowMulti, bool req = false)
    {
        type = t;
        direction = hd;
        allowMultiple = allowMulti;
        required = req;
    }
}
public enum HandleDirection
{
    IN,
    OUT
}
#endif