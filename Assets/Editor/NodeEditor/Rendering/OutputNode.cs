﻿using UnityEngine;
[Node("Utility/Output", typeof(OutputNode), typeof(Node))]
public class OutputNode : Node
{
    public string outputName;
    [Handle(typeof(object), HandleDirection.IN, true, false)]
    public object input;
    public OutputNode() : base()
    {
        rect = new Rect(0, 0, 100, 20);
    }
    public override void Draw()
    {
        base.Draw();
        GUI.Box(rect, new GUIContent(outputName));
    }
}