﻿#if UNITY_EDITOR
using System;
[AttributeUsage(AttributeTargets.Method)]
public class MapFunctionAttribute : Attribute
{
    public string buttonText;
    public MapFunctionAttribute(string bt)
    {
        buttonText = bt;
    }
}
#endif
