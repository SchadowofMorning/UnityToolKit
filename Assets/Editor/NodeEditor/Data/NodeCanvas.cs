﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
[Serializable, CreateAssetMenu(fileName = "cvs", menuName = "NodeEditor/Canvas", order = 1)]
public class NodeCanvas : ScriptableObject
{
    [SerializeField]
    private string path;
    [SerializeField]
    protected List<ScriptableObject> nodes;
    [SerializeField]
    public List<Type> baseTypes;
    [SerializeField]
    public bool changed;

    public int Count
    {
        get
        {
            return nodes.Count;
        }
    }
    public void Recalculate()
    {
        foreach (Node n in nodes)
        {
            n.CollectInputs();
            n.Recalculate();
        }
        changed = false;
    }
    public NodeCanvas()
    {
        nodes = new List<ScriptableObject>();
        baseTypes = new List<Type>();
        baseTypes.Add(typeof(Node));
    }
#if UNITY_EDITOR
    public void Draw()
    {   
        foreach (Node n in nodes)
        {
            n.Draw();
        }
    }
    public void AddNode(Node n)
    {
        nodes.Add(n);
    }
    public void DeleteNode(Node n)
    {
        nodes.Remove(n);
        foreach(Handle h in n.handles)
        {
            foreach(HandleConnection connection in h.connections)
            {
                DestroyImmediate(connection, true);
            }
            DestroyImmediate(h, true);
        }
        DestroyImmediate(n, true);
        changed = true;
        AssetDatabase.SaveAssets();
        SaveCanvas();
    }
    public void Drag(Vector2 delta)
    {
        foreach(Node n in nodes)
        {
            n.Drag(delta);
        }
    }
    public void RemoveAll()
    {
        while (Count > 0) DeleteNode((Node)nodes.First());
    }
    public bool HandleEvents(Event e)
    {
        if (nodes == null) nodes = new List<ScriptableObject>();
        try
        {
            foreach(Node n in nodes)
            {
                if (n.HandleEvents(e)) return true;
            }
        } catch(Exception exception)
        {
            Debug.LogWarning(exception.Message);
            for (int i = 0; i < nodes.Count; i++)
            {
                if (nodes[i] == null) nodes.RemoveAt(i);
            }
        }
        return false;
    }
    public void SetConstant(string s, object value)
    {
        foreach(Node n in nodes)
            if(n is ConstantNode)
            {
                ConstantNode c = n as ConstantNode;
                if(c.constantName == s)
                {
                    c.ChangeValue(value);
                }
            }
    }
    public void RevertConstants()
    {
        foreach (Node n in nodes)
        {
            if(n is ConstantNode)
            {
                ConstantNode c = n as ConstantNode;
                c.Revert();
            }
        }
    }
    public Dictionary<string, object[]> FetchOutputs()
    {
        Recalculate();
        Dictionary<string, List<object>> results = new Dictionary<string, List<object>>();
        foreach (Node n in nodes)
        {
            if (n is OutputNode)
            {
                OutputNode o = n as OutputNode;
                if (results.Keys.Contains(o.outputName))
                {
                    results[o.outputName].Add(o.input);
                }
                results.Add(o.outputName,new List<object>(new object[] { o.input }));
            }
        }
        Dictionary<string, object[]> res = new Dictionary<string, object[]>();
        foreach(KeyValuePair<string, List<object>> kvp in results)
        {
            res.Add(kvp.Key, kvp.Value.ToArray());
        }
        return res;
    }
    public List<NodeAttribute> GetNodesOfBaseType()
    {
        List<NodeAttribute> attributes = new List<NodeAttribute>();
        Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
        foreach (Assembly assembly in assemblies.Where(x => !x.GlobalAssemblyCache))
        {
            foreach(Type type in assembly.GetTypes())
            foreach (NodeAttribute cad in type.GetCustomAttributes<NodeAttribute>())
            {
                if (baseTypes.Contains(cad.baseType))
                {
                    attributes.Add(cad);
                }
            }
        }
        return attributes;
    }
    public void SaveCanvas()
    {
        path = AssetDatabase.GetAssetPath(this);
        Debug.Log(path);
        AssetDatabase.LoadAllAssetsAtPath(path).Where(x => x is HandleConnection && ((x as HandleConnection).origin == null || (x as HandleConnection).target == null)).ToList().ForEach(x => DestroyImmediate(x, true));
        for (int i = 0; i < nodes.Count; i++)
        {
            Node node = (Node)nodes[i];
            node.name = string.Format("-- {0} - {1}", node.GetType(), i);
            if(node.path != path)
            {
                node.path = path;
                AssetDatabase.AddObjectToAsset(node, path);
            }
            for (int j = 0; j < node.handles.Count; j++)
            {
                Handle handle = node.handles[j];
                handle.name = string.Format("--- {0} - {1}", node.name, j);
                if (handle.path != path)
                {
                    handle.path = path;
                    AssetDatabase.AddObjectToAsset(handle, path);
                }
                for (int k = 0; k < handle.connections.Count; k++)
                {
                    HandleConnection connection = handle.connections[k];
                    if (connection == null) continue;
                    connection.name = string.Format("---- {0} - {1}", handle.name, k);
                    if (connection.path != path)
                    {
                        connection.path = path;
                        AssetDatabase.AddObjectToAsset(connection, path);
                    }
                }
            }
        }
        AssetDatabase.SaveAssets();
    }
#endif
}