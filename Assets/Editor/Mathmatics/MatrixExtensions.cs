﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public static class MatrixExtensions
{
    public static float[,] GetLocalArea(this float[,] input, int xPos, int yPos, int xSize, int ySize)
    {
        float[,] output = new float[xSize, ySize];
        for (int x = 0; x < xSize; x++)
        {
            for (int y = 0; y < ySize; y++)
            {
                output[x, y] = input[Mathf.Clamp(xPos, 0, input.GetUpperBound(0)) + x, Mathf.Clamp(yPos, 0, input.GetUpperBound(1)) + y];
            }
        }
        return output;
    }
    public static float[,] GetConvexArea(this float[,] input, int xPos, int yPos, int size)
    {
        float[,] output = new float[size, size];
        int offset = size - (size % 2) / 2;
        for (int x = -offset; x <= offset; x++)
        {
            for (int y = -offset; y <= offset; y++)
            {
                output[x - offset, y + offset] = input[Mathf.Clamp(x + offset, 0, input.GetUpperBound(0)), Mathf.Clamp(y + offset, 0, input.GetUpperBound(1))];
            }
        }
        return output;
    }
    public static float[,] MultiplyEach(this float[,] input, float[,] other)
    {
        float[,] output = new float[input.GetLength(0), input.GetLength(1)];
        for (int x = 0; x < input.GetLength(0); x++)
        {
            for (int y = 0; y < input.GetLength(1); y++)
            {
                output[x, y] = input[x, y] * other[x, y];
            }
        }
        return output;
    }
    public static float CalculateConvolution(this float[,] input, int xPos, int yPos, float[,] kernel, int kernelsize)
    {
        float[,] subsegment = input.GetConvexArea(xPos, yPos, kernelsize);
        float[,] result = subsegment.MultiplyEach(kernel);
        return result.Cast<float>().Sum();
    }
}
