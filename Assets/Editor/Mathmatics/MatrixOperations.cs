﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class MatrixOperations
{
    public static void Convolute(float[,] input, int size) {
        float[,] output = new float[input.GetUpperBound(0), input.GetUpperBound(1)];
        for (int x = 0; x <= input.GetUpperBound(0); x++)
        {
            for (int y = 0; y <= input.GetUpperBound(1); y++)
            {
                float[,] matrix = input.GetConvexArea(x, y, size);
            }
        }
    }
    private static float CalculateConvolution(float[,] input, float[][,] convexes)
    {
        float totalweight = 0;
        for (int i = 0; i < convexes.Length; i++)
        {
            float weight = 0;
            for (int x = 0; x <= input.GetUpperBound(0); x++)
            {
                for (int y = 0; y <= input.GetUpperBound(1); y++)
                {
                    weight = input[x, y] * convexes[i][x, y];
                }
            }
            totalweight += weight;
        }
        return totalweight;
    }
    private static float[,] xSobel
    {
        get
        {
            return new float[,]
            {
            { -1, 0, 1 },
            { -2, 0, 2 },
            { -1, 0, 1 }
            };
        }
    }

    //Sobel operator kernel for vertical pixel changes
    private static float[,] ySobel
    {
        get
        {
            return new float[,]
            {
            {  1,  2,  1 },
            {  0,  0,  0 },
            { -1, -2, -1 }
            };
        }
    }
}