﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {
    public float speed;
    public float lerp;
    // Use this for initialization
    void Start () {
        StartCoroutine(Rotation());
	}
	
	// Update is called once per frame
	IEnumerator Rotation () {
        while (true)
        {
            transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, transform.eulerAngles += new Vector3(0, speed), lerp);
            yield return new WaitForFixedUpdate();
        }
	}
}
