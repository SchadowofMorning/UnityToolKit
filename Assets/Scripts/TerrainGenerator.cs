﻿
using System;
using System.Collections;
using System.Linq;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour
{
    [SerializeField]
    private NodeCanvas canvas;
    [SerializeField]
    private int Size;
    [SerializeField]
    private int Scale;
    [SerializeField]
    private Gradient gradient;
    [SerializeField]
    private float RoadCutoff;

    private MeshFilter filter;
    private GameObject[,] baseLayer;
    private GameObject[,] roadLayer;
    private float[,] baseMap;
    private float[,] roadMap;
    public void Start()
    {
        filter = GetComponent<MeshFilter>();
        GetInputs();
        StartCoroutine(CreateMap());
    }
    private void GetInputs()
    {
        canvas.SetConstant("Size", Size);
        canvas.Recalculate();
        var output = canvas.FetchOutputs();
        baseMap = (float[,])output["baseMap"].First();
        roadMap = (float[,])output["roads"].First();
    }
    public IEnumerator CreateMap()
    {
        for (int x = 0; x < Size; x++)
        {
            for (int y = 0; y < Size; y++)
            {
                GameObject g = GameObject.CreatePrimitive(PrimitiveType.Cube);
                g.transform.parent = transform;
                g.transform.position = new Vector3(x, baseMap[x, y] * Scale, y);
                g.GetComponent<Renderer>().material.color = gradient.Evaluate(baseMap[x, y]);
                if(roadMap[x,y] > 0.4f)
                {
                    GameObject r = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    r.transform.localScale = new Vector3(1, 0.2f, 1);
                    r.transform.localPosition = new Vector3(x,baseMap[x,y] * Scale + 0.5f , y);
                    r.transform.parent = g.transform;
                    r.GetComponent<Renderer>().material.color = Color.grey;
                }
            }
                yield return new WaitForSeconds(0.05f);
        }
    }
}
