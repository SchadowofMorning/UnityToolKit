# UnityToolKit
## Extendable Node Editor
A Node Editor running in the UnityEditor, useful for all kinds of stuff like
- Noisefunctions and their operants
- Quest or Storyline Editors
- Ability Execution Graphs
- and much more!

## Examples
this simple nodegraph creates a terrain and roads dependent on it
![nodegraph](readme/example node graph.PNG "nodegraph")
rendered as cubes this is the result:
![generated map](readme/generatedmap.PNG "generated map")

## TODO
- ~~Automatic retrieval of values from nodes, this has to be done manually at this point~~
- ~~Implementing a second execution path which does not calculate any GUI related events~~
- ~~implementing preprocessor arguments to be able to compile this~~?
- ~~finding a serializable solution for types including AssemblyNames to avoid collisions~~
- Writing extensive documentation
- tidy up the code in general
